﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

namespace RenameTool
{
    public partial class Form1 : Form
    {
        List<FileItem> items = new List<FileItem>();

        public Form1()
        {
            InitializeComponent();
        } 

        private void LoadFiles_Click(object sender, EventArgs e)
        {
            items.Clear();
            listFileNames.Items.Clear();

            var files = Directory.GetFiles(textFolder.Text);
            foreach (var file in files)
            {
                var item = new FileItem(file);
                items.Add(item);
                listFileNames.Items.Add(item.ListViewItem);
            }
        }

        private void Endfix_Changed(object sender, EventArgs e)
        {
            foreach (var item in items)
            {
                item.PreviewName(textPrefix.Text, textPostfix.Text);
            }
        }

        private void buttonDoRename_Click(object sender, EventArgs e)
        {
            foreach (var item in items)
                item.Rename(textPrefix.Text, textPostfix.Text
                    );
        }
    }

    class FileItem
    {
        public FileInfo Info { get; private set; }

        public ListViewItem ListViewItem { get; private set; }

        public void PreviewName(string prefix, string postfix)
        {
            ListViewItem.SubItems[1].Text = prefix + Path.GetFileNameWithoutExtension(Info.FullName) + postfix + Path.GetExtension(Info.FullName);
        }

        public FileItem(string filename)
        {
            Info = new FileInfo(filename);
            ListViewItem = new ListViewItem(new string[] { Path.GetFileName(filename), Path.GetFileName(filename), "Not yet renamed." });
        }

        public void Rename(string prefix, string postfix)
        {
            try
            {
                Info.MoveTo(Path.Combine(
                    Info.DirectoryName,
                    prefix + Path.GetFileNameWithoutExtension(Info.FullName) + postfix + Path.GetExtension(Info.FullName)
                    ));
                ListViewItem.SubItems[2].Text = "Renamed OK";
            }
            catch (Exception e)
            {
                ListViewItem.SubItems[2].Text = "Renamed error: " + e.Message;
            }

        }
    }
}
